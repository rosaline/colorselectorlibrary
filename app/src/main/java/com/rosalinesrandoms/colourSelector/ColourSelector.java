package com.rosalinesrandoms.colourSelector;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.rosalinesrandoms.colorSelector.R;


public class ColourSelector extends RelativeLayout {
	//The paint selection to draw the colour selected
	private Paint paint = new Paint();
	
	//The seek bars of the hue and value to do the selecting
	protected SeekBar hue;
	protected SeekBar value;
	
	//The drawn rectangle of the colour
	private ShapeDrawable hueShape;
	private ShapeDrawable valueShape;
    private ColorButton colorButton;
	
	//The hue given from the latest selection
	protected int currentHue = 0xFFFF0000;
	//The full current colour of the hue and value combined
	private int currentColor = 0xFFFF0000;
	
	//The startup progress of the value - default to 15 or neutral until anything is selected
	private int valueProgress = 15;
	private int hueProgress = 0;
	
	//The width of the seek bars
	//private int gradientWidth;
    private int height = 128;
    private int width = 256;
    private int bar_width = 192;
    private int bar_height = 34;
    private int selector_width = 8;
    private int selector_height = 32;
    private int gray_bar_width = 13;
    private int hue_bar_width = 197;
    private int bar_offset = 4;

    //define the radius for the rounded corners on the board
    private int r=45;
    private float[] outerRadius =new float[]{r,r,r,r,r,r,r,r};

    //use a flag so we are not re-drawing the hue every time we change the color
    private boolean hueDrawn = false;
	
	//The background gradient for each seek bar
	private LinearGradient hueGradient;
	private LinearGradient valueGradient;

    private int dpToPxMultiple = 1;

	 public ColourSelector(Context context)
	    {
	    this(context, null);

	    }
	    public ColourSelector(Context context, AttributeSet attrs) {
	    this(context, attrs,0);

	    }
	    
	/**
	 * A colour selector that has a hue and value gradient with a box showing the full colour shown
	 * @param context
	 * @param attrs
	 */
	public ColourSelector(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.colour_selector, this);
		this.setWillNotDraw(false);

		//find the seek bars to be used
		hue = (SeekBar) findViewById(R.id.hue_selector);
		value = (SeekBar) findViewById(R.id.value_selector);
        colorButton = (ColorButton) findViewById(R.id.space);

        //set the seek bars so there is padding on the right for the color circle
        value.setPadding(0,0,height,0);
        hue.setPadding(0,0,height,0);

		hueShape = new ShapeDrawable(new RoundRectShape(outerRadius,null,null));
		valueShape = new ShapeDrawable(new RoundRectShape(outerRadius,null,null));

		//Create a change listener for the hue, to change the colour selected and redraw on each change
		hue.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				//Find the position of the seek bar, convert that to a colour and force the widget to redraw
				currentHue = hueSelection(progress);
				hueProgress = progress;

             	colourSelection(valueProgress);
				invalidate();
			}

			//We don't care about any tracking unless there is a progress change
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
			}
		});

		//Create a change listener for the value
		value.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				//set the valueProgress for the hue to use if necessary, and find the current colour again then force a redraw
				valueProgress = progress;
				colourSelection(progress);
				invalidate();
			}

			//we don't care about a change unless the progress changes
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

		});

        colorButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("color", "click");
            }
        });

	}

    private void drawHue(){
        float finalValue = 105f/90f;
        //Create the gradients
        hueGradient = new LinearGradient(0.f, 0.f, hue_bar_width, 0.f, new int[] {
                0xFFFF0000, 0xFFFF00FF, 0xFF0000FF, 0xFF00FFFF, 0xFF00FF00,
                0xFFFFFF00, 0xFFFF0000}, new float[] {
                0f, (15f / 105f)*finalValue, (30f / 105f)*finalValue, (45f / 105f)*finalValue, (60f / 105f)*finalValue, (75f / 105f)*finalValue,(90f / 105f)*finalValue
        }, TileMode.REPEAT);
        //set the gradients
        hueShape.getPaint().setShader(hueGradient);

        RectShape rect = new RoundRectShape(outerRadius, null, null);
        ShapeDrawable newRect = new ShapeDrawable(rect);
        newRect.getPaint().setColor(Color.GRAY);

        //create a layer shape so we can make a single drawable with the gradient and grey bars together
        LayerDrawable compoundHueShape = new LayerDrawable(new Drawable[] {hueShape, newRect});
        compoundHueShape.setLayerInset(0,0,bar_offset,gray_bar_width,bar_offset);
        compoundHueShape.setLayerInset(1,hue_bar_width,bar_offset,0,bar_offset );

        //draw the hue background
        hue.setProgressDrawable((Drawable) compoundHueShape);
    };



    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        width = pixelsToDp(128);
        if(getLayoutParams().width==LayoutParams.WRAP_CONTENT)
        {

        }
        else if((getLayoutParams().width==LayoutParams.MATCH_PARENT)||(getLayoutParams().width==LayoutParams.FILL_PARENT))
        {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }
        else
            width = getLayoutParams().width;


        height = pixelsToDp(64);
        if(getLayoutParams().height==LayoutParams.WRAP_CONTENT)
        {

        }
        else if((getLayoutParams().height==LayoutParams.MATCH_PARENT)||(getLayoutParams().height==LayoutParams.FILL_PARENT))
        {
            height = MeasureSpec.getSize(heightMeasureSpec);
        }
        else
            height = getLayoutParams().height;

        //calculate the size of all parts of the bars as a function of the height
        bar_height = (height/8)*3;
        selector_width = height/8;
        selector_height = height/2;
        bar_width = width - height - selector_width;
        gray_bar_width = (bar_width)/7;
        hue_bar_width = bar_width - gray_bar_width;
        bar_offset = height/16;

        //create padding around the bars, for the selector width to overdraw, and to draw the circle by the end
        //LayoutParams params = new LayoutParams(
        //        LayoutParams.WRAP_CONTENT,
        //        LayoutParams.WRAP_CONTENT
        //);


        value.setPadding(selector_width/2,0,selector_width/2,0);
        hue.setPadding(selector_width/2,0,selector_width/2,0);

        value.setMinimumHeight(width-height);
        hue.setMinimumWidth(width-height);
        colorButton.setMinimumWidth(height);

        //Set the margin to the right of the bars so they do not overlap the circle
        //MarginLayoutParams params = (MarginLayoutParams) hue.getLayoutParams();
        //params.rightMargin = height;
        //MarginLayoutParams params2 = (MarginLayoutParams) value.getLayoutParams();
        //params2.rightMargin = height;



        getLayoutParams().height = height;
        getLayoutParams().width = width;

        super.onMeasure((widthMeasureSpec), heightMeasureSpec);
    }

    /**
     * Convert the wanted dp to the pixels
     * @param dp - wanted density independent pixels
     * @return - the px expression
     */
    private int pixelsToDp(int dp) {
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int)px;
    }



    /**
	 * Draw the backgrounds of the seek bars as well as the final colour selection
	 */
	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

        //make sure we are not constantly re-drawing the hue for no reason
        //it only needs to be redrawn when the layout is changed
        if(!hueDrawn) {
            hueDrawn = true;
            drawHue();
        }

        //calculate the background gradient for the value and set it with the hue
        //TODO: This could be made more efficient if we were keeping track of if the hue or gradient were changed, and only re-setting the colors for the hue
		valueGradient = new LinearGradient(0.f, 0.f,
				bar_width, 0.0f,
				new int[] { Color.WHITE, currentHue, 0xFF000000 }, null,
				TileMode.CLAMP);

		valueShape.getPaint().setShader(valueGradient);
        valueShape.setBounds(0,bar_offset,bar_width,bar_height+bar_offset);
        value.setProgressDrawable((Drawable) valueShape);

        //set the thumbs to the current color
        value.getThumb().setColorFilter(currentHue, PorterDuff.Mode.MULTIPLY);
        hue.getThumb().setColorFilter(currentHue, PorterDuff.Mode.MULTIPLY);
        colorButton.setColor(currentColor);

        drawColour(canvas);
	}

    /**
     * Re-draw the color circle
     * @param drawCanvas
     */
    protected void drawColour(Canvas drawCanvas) {

        //draw the background color
        //colorButton.getBackground().setColorFilter(currentColor, PorterDuff.Mode.MULTIPLY);

        //save this code- it is impossible to add the drop shadow in XML, so we may switch back to drawing in code again at some point
        //Re-draw the color circle
        //paint.setColor(currentColor);
        //set a background shadow for the circle
        //paint.setShadowLayer(8, 5, 5, Color.DKGRAY);
        //setLayerType(LAYER_TYPE_SOFTWARE, paint);
        //drawCanvas.drawCircle(width - (height / 2), (height / 2) - 8, height / 2 - 18, paint);

    }


	/**
	 * Find the hue from the first bar, using the progress
	 * @param progress
	 * @return
	 */
	protected int hueSelection(int progress) {
		// there are 7 colour stops including the end points
		// determine which section we are looking at - rounded down to the
		// nearest stop
		int section = progress / 15;
		int section_division = progress - (section * 15);
		int section_color;
		int division_color = 17 * section_division; // a number between 0 and

		switch (section) {
		case 0:// 0-15
				// Colour is between 0xffff0000 and 0xffff00ff
				// only the last two digits change
			section_color = 0xFFFF0000 + division_color;
			break;
		case 1:// 15-30
				// give the base color that is not changed
			section_color = 0xFF0000FF + (~division_color << 16);
			break;
		case 2:// 30-45
			section_color = 0xFF0000FF + (division_color << 8);
			break;
		case 3:// 45-60
			section_color = 0xFF00FF00 + (~division_color);
			break;
		case 4:// 60-75
			section_color = 0xFF00FF00 + (division_color << 16);
			break;
		case 5:// 75-90
			section_color = 0xFFFF0000 + (~division_color << 8);
			break;
		default: // case 6 and 7 - (90-105)
			section_color = 0xFF888888;
			break;

		}

		return section_color;
	}

	/**
	 * Using the already determined Hue, determine the full colour by the value
	 * @param progress
	 * @return
	 */
	// bar
	private int colourSelection(int progress) {
		
		//If it is 15 we will use the true colour
		if (progress == 15) {
			currentColor = currentHue;
			return currentHue;
			//Under 15 we scroll down closer and closer to white
		} else if (progress < 15) {
			int red = add_progress(Color.red(currentHue), (15 - progress));
			int green = add_progress(Color.green(currentHue), (15 - progress));
			int blue = add_progress(Color.blue(currentHue), (15 - progress));
			currentColor = Color.argb(255, red, green, blue);
			//15-30 we will scroll up to black
		} else if (progress > 15) {
			int red = sub_progress(Color.red(currentHue), (progress - 15));
			int green = sub_progress(Color.green(currentHue), (progress - 15));
			int blue = sub_progress(Color.blue(currentHue), (progress - 15));
			currentColor = Color.argb(255, red, green, blue);
		}
		
		//return the full colur that will be moved
		return currentColor;
	}

	/**
	 * Adds the step increment to the current colour segment proportional to how
	 * the bar has moved
	 * 
	 * @param ColorSection
	 *            - an int representation of the colour as it is raw
	 * @param progress_steps
	 *            - a number between 0 and 15 for how far the progress bar has
	 *            progressed
	 * @return
	 */
	protected int add_progress(int ColorSection, int progress_steps) {
		int change = ((255 - ColorSection) * progress_steps) / 15;
		return (ColorSection + change);
	}

	/**
	 * Adds the step increment to the current colour segment proportional to how
	 * the bar has moved
	 * 
	 * @param ColorSection
	 *            - an int representation of the colour as it is raw
	 * @param progress_steps
	 *            - a number between 0 and 15 for how far the progress bar has
	 *            progressed
	 * @return
	 */
	private int sub_progress(int ColorSection, int progress_steps) {
		int change = ((ColorSection) * progress_steps) / 15;
		return (ColorSection - change);
	}
	
    /**
     * Set the colour box to the color specified, and set the bars to the closest
     * values that we can represent with our sliders
     * @param inputColor
     */
    public void setColor(int inputColor){
        setHue(inputColor);
        setValue(inputColor);
        currentColor = inputColor;
        invalidate();
    }

    /**
     * Set the hue to the color we are specifying, without affecting the value.
     * This allows us to change a blue to a green (for example) keeping the same shade
     * It will always find the closest hue to the color specified, and use that as the true color
     * @param inputColor
     */
    public void setHue(int inputColor){
        currentHue = calculateHue(inputColor);

        int position = calculateHuePosition(currentHue);
        hue.setProgress(position);

        //force a redraw of the bar in case the hue has changed
        invalidate();
    }

    /**
     * For a given hue color, calculate the position of the seek bar
     * @param currentHue - An integer color that has already been converted to a 'hue' color - all but 1 numbers are either 255 or 0
     * @return the integer position of the Hue (top) seek bar
     */
    private int calculateHuePosition(int currentHue){
        int red = Color.red(currentHue);
        int green = Color.green(currentHue);
        int blue = Color.blue(currentHue);

        //if they are all equal - it belongs in the gray-scale section
        if((red == green) && (red == blue)) {
            return 100;
        }

        if(red == 255)
        {
            if(green == 0)
                return blue/17;
            else
            {
                return 90-green/17;
            }
        }
        if(green == 255) {
            if (red == 0)
                return (60 - blue / 17);
            else
                return (60 + red / 17);
        }
        else {
            if (red == 0)
                return (30 + green / 17);
            else
                return 30 - (red / 17);
        }

    }

    /**
     * Taking any colour convert it into a hue colour - where all but 1 colour is a 0 or 255
     * @param inputColor
     * @return - the hue representation of the color
     */
    public int calculateHue(int inputColor){
        int red = Color.red(inputColor);
        int green = Color.green(inputColor);
        int blue = Color.blue(inputColor);

        int hue = 0;
        final int initialColor = 128;  //define a constant color to initialize unset colors to

        /* initialize the numbers to anything that is NOT 0 or 255, so we can tell if they are set in the initial steps or not */
        int outputRed = initialColor;
        int outputGreen = initialColor;
        int outputBlue = initialColor;

        /* find the lowest input color of the three - that will have a hue of 0 */
        if((red <= green) && (red <= blue))
            outputRed = 0;
        if((green <= red) && (green <= blue))
            outputGreen = 0;
        if((blue <= red) && (blue <= green))
            outputBlue = 0;

        //find the highest input color of the three - that will have a hue of 255
        if((red >= green) && (red >= blue))
            outputRed = 255;
        if((green >= red) && (green >= blue))
            outputGreen = 255;
        if((blue >= red) && (blue >= green))
            outputBlue = 255;

        /*
        Unless there have been colors identical (which will happen in which case this is ignorable)
        We have one last color to set - and it will be set to an interpolation between the other two,
        using the same ratio it was set between the two prior
        */
        if(outputRed == 128)
        {
            if(green > blue)
                outputRed = interpolateColor(green, blue, red);
            else if(blue > green)
                outputRed = interpolateColor(blue, green, red);
        }
        if(outputGreen == 128)
        {
            if(red > blue)
                outputGreen = interpolateColor(red, blue, green);
            else if(blue > red)
                outputGreen = interpolateColor(blue, red, green);
        }
        if(outputBlue == 128)
        {
            if(green > red)
                outputBlue = interpolateColor(green, red, blue);
            else if(red > green)
                outputBlue = interpolateColor(red, green, blue);
        }

        hue = Color.argb(255,outputRed, outputGreen, outputBlue);
        return hue;
    }


    /**
     * Use cross multiplication to discover what the middle colour should be holding the same ratio
     * when the high and low are streached to 0 and 255
     * @param high
     * @param low
     * @param wanted
     * @return
     */
    private int interpolateColor(int high, int low, int wanted){
        float interpolatedColor = 0;
        interpolatedColor = (((float)wanted-(float)low)/((float)high-(float)low))*255;

        return (int) interpolatedColor;
    }

    /**
     * Using the difference between the wanted colour, and the hue of the colour calculate the value bar.
     * The position determines how much black or white is included in the colour.
     * @param inputColor - the colour we are trying to represent
     */
    public void setValue(int inputColor){

        //calculate the hue of the input color
        int inputHue = calculateHue(inputColor);

        float redOffset;
        float greenOffset;
        float blueOffset;

        int inputRed = Color.red(inputColor);
        int inputGreen = Color.green(inputColor);
        int inputBlue = Color.blue(inputColor);

        int hueRed = Color.red(inputHue);
        int hueGreen = Color.green(inputHue);
        int hueBlue = Color.blue(inputHue);

        int position = 0;

        //find the difference between all the wanted colours and the hue colours
        redOffset = (inputRed - hueRed)/15;
        blueOffset = (inputBlue - hueBlue)/15;
        greenOffset = (inputGreen - hueGreen)/15;

        //determine if we are going up the scale (towards black) or down (towards white)
        int totalOffset = (int)redOffset+(int)blueOffset+(int)greenOffset;

        //if it is a positive offset, look for the largest offset and subtract that from the midpoint
        //of the bar
        if(totalOffset >= 0) {
            int posOffset = (int) Math.max(redOffset, Math.max(blueOffset, greenOffset));
            position = 15 - (posOffset);
        }
        //if it is a negative offset, find the smallest number and subtract that (so double negative - we are going to
        //be more than 15) to the center point of the bar - for a number between the mid point and the top
        else{
            int negOffset = (int) Math.min(redOffset, Math.min(blueOffset, greenOffset));
            position = 15 - (negOffset);
        }

        //set the position and force a redraw
        value.setProgress(position);
        invalidate();
    }

    /**
     *
     * @return The integer of the colour selected
     */
    public int getColor(){
        return currentColor;
    }

	/**
	 * 
	 * @return The integer of the hue selected
	 */
	public int getHue(){
		return currentHue;
	}
	
	/**
	 * 
	 * @return the progress of the hue slider
	 */
	public int getHuePosition(){
		return hueProgress;
	}
	
	/**
	 * 
	 * @return the progress of the value slider
	 */
	public int getValuePosition(){
		return valueProgress;
	}
	
	/**
	 * Set the value of the hue progress bar
	 * @param progress
	 */
	public void setHueProgress(int progress){
		hue.setProgress(progress);
		}

	/**
	 * Set the value of the value progress bar
	 * @param progress
	 */
	public void setValueProgress(int progress){
		value.setProgress(progress);
	}
}
