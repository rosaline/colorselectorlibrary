package com.rosalinesrandoms.colourSelector;



import com.rosalinesrandoms.colorSelector.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class SearchSelector extends ColourSelector {

	// The paint selection to draw the colour selected
	private Paint paint = new Paint();

	// The seek bars of the hue and value to do the selecting
	private SeekBar hueMax;
	private SeekBar valueMax;

	private int hueMaxProgress = 50;
	private int valueMaxProgress = 15;

	private int hueIntermediateProgress = 50;
	private int valueIntermediateProgress = 15;

	public SearchSelector(Context context) {
		this(context, null);

	}

	public SearchSelector(Context context, AttributeSet attrs) {
		this(context, attrs, 0);

	}

	public SearchSelector(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.colour_search, this);
		this.setWillNotDraw(false);

		// find the seek bars to be used
		hueMax = (SeekBar) findViewById(R.id.hue_selector2);
		valueMax = (SeekBar) findViewById(R.id.value_selector2);

		// Create a change listener for the hue, to change the colour selected
		// and redraw on each change
		hueMax.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

				if (progress < hueIntermediateProgress) {
					hue.setProgress(progress);
					seekBar.setProgress(hueMaxProgress);
					hueIntermediateProgress = (hueMaxProgress - progress) / 2
							+ progress;
				} else {
					hueMaxProgress = progress;
					hueIntermediateProgress = (progress - hue.getProgress())
							/ 2 + hue.getProgress();
				}
				invalidate();
			}

			// We dont care about any tracking unless there is a progress change
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}
		});

		// Create a change listener for the hue, to change the colour selected
		// and redraw on each change
		valueMax.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

				if (progress < valueIntermediateProgress) {
					value.setProgress(progress);
					seekBar.setProgress(valueMaxProgress);
					valueIntermediateProgress = (valueMaxProgress - progress)
							/ 2 + progress;
				} else {
					valueMaxProgress = progress;
					valueIntermediateProgress = (progress - value.getProgress())
							/ 2 + value.getProgress();
				}
				invalidate();
			}

			// We dont care about any tracking unless there is a progress change
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	/**
	 * Draw the colour block with the colour found from the progress bars
	 * 
	 * @param drawCanvas
	 */
	@Override
	protected void drawColour(Canvas drawCanvas) {
		LinearGradient valueBoxGradient;
		LinearGradient valueBoxGradientShader;
		// Find the size of the colour box
		// ### THIS IS CURRENTLY HARD CODED
		int width = drawCanvas.getWidth();
		int height = drawCanvas.getHeight();
		paint.setStrokeWidth(3);
		// these rect values are in dip not px! yay!
		drawCanvas
				.drawRect(width - 2 - height, 2, width - 2, height - 2, paint);
		paint.setStrokeWidth(0);

		// currentHue,hueSelection(hueMaxProgress)
		valueBoxGradientShader = new LinearGradient(0.0f, 5, 0.0f, height - 5,
				getHueGradient(), null, TileMode.CLAMP);
		Paint paint1 = new Paint();
		paint1.setShader(valueBoxGradientShader);

		drawCanvas.drawRect(width - height, 5, width - 5, height - 5, paint1);

		Paint paint2 = new Paint();
		// Colour in the box
		// paint.setShader(valueBoxGradient);
		valueBoxGradient = new LinearGradient(width - height, 0.0f, width - 5,
				0.0f, getShadowGradient(), null, TileMode.CLAMP);
		paint2.setShader(valueBoxGradient);
		// paint.setColor(0x880000ff);

		drawCanvas.drawRect(width - height, 5, width - 5, height - 5, paint2);
	}

	private int[] getHueGradient() {
		int change = 5;
		int hueDifference = (hueMaxProgress - getHuePosition()) / change;

		int[] hueProgressArray = new int[hueDifference + 2];
		hueProgressArray[0] = getHue();

		for (int i = 1; i <= hueDifference; i++) {
			int selectionPosition = (change * i) + getHuePosition();
			hueProgressArray[i] = hueSelection(selectionPosition);
		}

		hueProgressArray[hueDifference + 1] = hueSelection(hueMaxProgress);
		return hueProgressArray;

	}

	private int[] getShadowGradient() {
		if (getValuePosition() < 15) {
			if (valueMaxProgress < 15)
				return new int[] { getShadowAtTransparency(getValuePosition()),
						getShadowAtTransparency(valueMaxProgress) };
			else
				return new int[] { getShadowAtTransparency(getValuePosition()),
						0x00ffffff, 0x00000000,
						getShadowAtTransparency(valueMaxProgress) };
		} else {
			if (valueMaxProgress > 15)
				return new int[] { getShadowAtTransparency(getValuePosition()),
						getShadowAtTransparency(valueMaxProgress) };
			else
				return new int[] { getShadowAtTransparency(getValuePosition()),
						0x00000000, 0x00ffffff,
						getShadowAtTransparency(valueMaxProgress) };
		}

	}

	private int getShadowAtTransparency(int hueProgress) {

		int progressDifference;
		if (hueProgress > 15)
			progressDifference = hueProgress - 15;
		else
			progressDifference = 15 - hueProgress;

		int darkness;

		darkness = (progressDifference) * (0xff / 15);

		// darkness = 0xff;
		if (hueProgress < 15)
			return Color.argb(darkness, 0xff, 0xff, 0xff);
		else
			return Color.argb(darkness, 0x00, 0x00, 0x00);

	}

	public String getColourSearch() {
		return ColourSelectorTable.COLUMN_HUE_SELECTOR + " >= "
				+ getHuePosition() + " AND "
				+ ColourSelectorTable.COLUMN_HUE_SELECTOR + " <= "
				+ hueMaxProgress + " AND "
				+ ColourSelectorTable.COLUMN_COLOUR_SELECTOR + " >= "
				+ getValuePosition() + " AND "
				+ ColourSelectorTable.COLUMN_COLOUR_SELECTOR + " <= "
				+ valueMaxProgress;

	}
}
