package com.rosalinesrandoms.colourSelector;



import com.rosalinesrandoms.colorSelector.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;


public class ColourSelector extends RelativeLayout {
	//The paint selection to draw the colour selected
	private Paint paint = new Paint();
	
	//The seek bars of the hue and value to do the selecting
	protected SeekBar hue;
	protected SeekBar value;
	
	//The drawn rectangle of the colour
	private ShapeDrawable hueShape;
	private ShapeDrawable valueShape;
	
	//The hue given from the latest selection
	protected int currentHue = 0xFFFF0000;
	//The full current colour of the hue and value combined
	private int currentColour = 0xFFFF0000;
	
	//The startup progress of the value - default to 15 or neutral until anything is selected
	private int valueProgress = 15;
	private int hueProgress = 0;
	
	//The width of the seek bars
	private int gradientWidth;
	
	//The background gradient for each seek bar
	private LinearGradient hueGradient;
	private LinearGradient valueGradient;

	 public ColourSelector(Context context)
	    {
	    this(context, null);

	    }
	    public ColourSelector(Context context, AttributeSet attrs) {
	    this(context, attrs,0);

	    }
	    
	/**
	 * A colour selector that has a hue and value gradient with a box showing the full colour shown
	 * @param context
	 * @param attrs
	 */
	public ColourSelector(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.colour_selector, this);
		this.setWillNotDraw(false);

		//find the seek bars to be used
		hue = (SeekBar) findViewById(R.id.hue_selector);
		value = (SeekBar) findViewById(R.id.value_selector);
		
		//the rectangle that will show the final colour selection
		hueShape = new ShapeDrawable(new RectShape());
		valueShape = new ShapeDrawable(new RectShape());

		//Create a change listener for the hue, to change the colour selected and redraw on each change
		hue.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				//Find the position of the seek bar, convert that to a colour and force the widget to redraw
				currentHue = hueSelection(progress);
				hueProgress = progress;
				colourSelection(valueProgress);
				invalidate();
			}

			//We dont care about any tracking unless there is a progress change
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}
		});

		
		//Create a change listener for the value
		value.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				//set the valueProgress for the hue to use if necessary, and find the current colour again then force a redraw
				valueProgress = progress;
				colourSelection(progress);
				invalidate();
				// TODO Auto-generated method stub

			}

			//we dont care about a change unless the progress changes
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		});

	}
	

	/**
	 * Draw the backgrounds of the seek bars as well as the final colour selection
	 * ###THIS IS NOT AS EFFICIENT AS IT COULD BE
	 */
	@SuppressLint("NewApi")
	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		//Find the width of the seek bar - it is the total width minus the width of the thumb because room is left for the thumb to overlap the edges
		gradientWidth = hue.getMeasuredWidth()
				- (hue.getThumb().getMinimumWidth());
		
		
		//Create the gradients
		//###This is not very efficient to redraw every time!
		hueGradient = new LinearGradient(0.f, 0.f, gradientWidth, 0.0f, new int[] {
				0xFFFF0000, 0xFFFF00FF, 0xFF0000FF, 0xFF00FFFF, 0xFF00FF00,
				0xFFFFFF00, 0xFFFF0000, 0xFF888888, 0xFF888888 }, new float[] {
				0f, 15f / 105f, 30f / 105f, 45f / 105f, 60f / 105f, 75f / 105f,
				(90f / 105f), (90f / 105f), 1f }, TileMode.REPEAT);
		valueGradient = new LinearGradient(0.f, 0.f,
				gradientWidth, 0.0f,
				new int[] { Color.WHITE, currentHue, 0xFF000000 }, null,
				TileMode.CLAMP);
		
		//set the gradients
		hueShape.getPaint().setShader(hueGradient);
		valueShape.getPaint().setShader(valueGradient);
		hue.setProgressDrawable((Drawable) hueShape);
		value.setProgressDrawable((Drawable) valueShape);
		
		
		//Draw the border around the square
		paint.setColor(Color.BLACK);

		//Find the current colour and redraw
		drawColour(canvas);
		

	}

	/**
	 * Draw the colour block with the colour found from the progress bars
	 * 
	 * @param drawCanvas
	 */
	protected void drawColour(Canvas drawCanvas) {
		//Find the size of the colour box
		//### THIS IS CURRENTLY HARD CODED
		int width = drawCanvas.getWidth();
		int height = drawCanvas.getHeight();
		paint.setStrokeWidth(3);
		// these rect values are in dip not px! yay!
		drawCanvas
				.drawRect(width - 2 - height, 2, width - 2, height - 2, paint);
		paint.setStrokeWidth(0);
		
		//Colour in the box
		paint.setColor(currentColour);
		drawCanvas.drawRect(width - height, 5, width - 5, height - 5, paint);
	}

	/**
	 * Find the hue from the first bar, using the progress
	 * @param progress
	 * @return
	 */
	protected int hueSelection(int progress) {
		// there are 7 colour stops including the end points
		// determine which section we are looking at - rounded down to the
		// nearest stop
		int section = progress / 15;
		int section_division = progress - (section * 15);
		int section_color;
		int division_color = 17 * section_division; // a number between 0 and

		switch (section) {
		case 0:// 0-15
				// Colour is between 0xffff0000 and 0xffff00ff
				// only the last two digits change
			section_color = 0xFFFF0000 + division_color;
			break;
		case 1:// 15-30
				// give the base color that is not changed
			section_color = 0xFF0000FF + (~division_color << 16);
			break;
		case 2:// 30-45
			section_color = 0xFF0000FF + (division_color << 8);
			break;
		case 3:// 45-60
			section_color = 0xFF00FF00 + (~division_color);
			break;
		case 4:// 60-75
			section_color = 0xFF00FF00 + (division_color << 16);
			break;
		case 5:// 75-90
			section_color = 0xFFFF0000 + (~division_color << 8);
			break;
		default: // case 6 and 7 - (90-105)
			section_color = 0xFF888888;
			break;

		}

		return section_color;
	}

	/**
	 * Using the already determined Hue, determine the full colour by the value
	 * @param progress
	 * @return
	 */
	// bar
	private int colourSelection(int progress) {
		
		//If it is 15 we will use the true colour
		if (progress == 15) {
			currentColour = currentHue;
			return currentHue;
			//Under 15 we scroll down closer and closer to white
		} else if (progress < 15) {
			int red = add_progress(Color.red(currentHue), (15 - progress));
			int green = add_progress(Color.green(currentHue), (15 - progress));
			int blue = add_progress(Color.blue(currentHue), (15 - progress));
			currentColour = Color.argb(255, red, green, blue);
			//15-30 we will scroll up to black
		} else if (progress > 15) {
			int red = sub_progress(Color.red(currentHue), (progress - 15));
			int green = sub_progress(Color.green(currentHue), (progress - 15));
			int blue = sub_progress(Color.blue(currentHue), (progress - 15));
			currentColour = Color.argb(255, red, green, blue);
		}
		
		//return the full colur that will be moved
		return currentColour;
	}

	/**
	 * Adds the step increment to the current colour segment proportional to how
	 * the bar has moved
	 * 
	 * @param ColorSection
	 *            - an int representation of the colour as it is raw
	 * @param progress_steps
	 *            - a number between 0 and 15 for how far the progress bar has
	 *            progressed
	 * @return
	 */
	protected int add_progress(int ColorSection, int progress_steps) {
		int change = ((255 - ColorSection) * progress_steps) / 15;
		return (ColorSection + change);
	}

	/**
	 * Adds the step increment to the current colour segment proportional to how
	 * the bar has moved
	 * 
	 * @param ColorSection
	 *            - an int representation of the colour as it is raw
	 * @param progress_steps
	 *            - a number between 0 and 15 for how far the progress bar has
	 *            progressed
	 * @return
	 */
	private int sub_progress(int ColorSection, int progress_steps) {
		int change = ((ColorSection) * progress_steps) / 15;
		return (ColorSection - change);
	}
	
	/**
	 * 
	 * @return The integer of the colour selected
	 */
	public int getColour(){
		return currentColour;
	}
	
	/**
	 * 
	 * @return The integer of the hue selected
	 */
	public int getHue(){
		return currentHue;
	}
	
	/**
	 * 
	 * @return the progress of the hue slider
	 */
	public int getHuePosition(){
		return hueProgress;
	}
	
	/**
	 * 
	 * @return the progress of the value slider
	 */
	public int getValuePosition(){
		return valueProgress;
	}
	
	/**
	 * Set the value of the hue progress bar
	 * @param valueProgress
	 */
	public void setHueProgress(int progress){
		hue.setProgress(progress);
		}

	/**
	 * Set the value of the value progress bar
	 * @param progress
	 */
	public void setValueProgress(int progress){
		value.setProgress(progress);
	}
}
