package com.rosalinesrandoms.colourSelector;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ColourSelectorTable {
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_COLOUR_HUE = "hue";
	public static final String COLUMN_COLOUR_SELECTED = "colour";
	public static final String COLUMN_HUE_SELECTOR = "hue_selector";
	public static final String COLUMN_COLOUR_SELECTOR = "colour_selector";

	public static final String TABLE = "colour_selector_table";
	public static final String QUALIFIED_ID = TABLE+"."+COLUMN_ID;

	// Database creation SQL statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_COLOUR_HUE
			+ " integer," + COLUMN_COLOUR_SELECTED + " integer,"
			+ COLUMN_HUE_SELECTOR + " integer," + COLUMN_COLOUR_SELECTOR
			+ " integer);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(ColourSelectorTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE);
		onCreate(database);
	}
}
